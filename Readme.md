﻿# FreeMarket WebView repository.

WebView is the Java application containing a Web browser with FreeMarket page.
This app facilitates the usage of the FreeMarket.

# Usage

Download the bundle for your platform, make sure your NXT client is running, run WebView.exe or WebView and enjoy.
If you do not have your platform listed here you can download the dist bundle and run it with run.sh or run.bat.
dist bundle requires java 1.8 installed on your system. Make sure your run.sh file has execute privileges or use shell command:

chmod +x run.sh

# Building

WebView built with java 1.8 and requires JavaFX 2.2.

You need this entries on the classpath to build FreeMarket WebView from source:

bcprov-jdk15on-151.jar

commons-logging-1.1.3.jar

httpclient-4.3.5.jar

httpcore-4.3.2.jar

jetty-continuation-9.2.2.v20140723.jar

jetty-http-9.2.2.v20140723.jar

jetty-io-9.2.2.v20140723.jar

jetty-security-9.2.2.v20140723.jar

jetty-server-9.2.2.v20140723.jar

jetty-servlet-9.2.2.v20140723.jar

jetty-servlets-9.2.2.v20140723.jar

jetty-util-9.2.2.v20140723.jar

json-simple-1.1.1.jar

NxtPass.jar

servlet-api-3.1.jar

slf4j-api-1.7.7.jar

slf4j-jdk14-1.7.7.jar

opencsv-3.3.jar

conf

site

resources


Select your platform in src/platform directory and execute do-deploy task for the build.xml file. Make sure you have java
runtime in the parent directory of the WebView. Java runtime should be in the folder WebView/../runtime.


# Platforms

Linux users should use Oracle Java 1.8 to use dist bundle:

sudo add-apt-repository ppa:webupd8team/java

sudo apt-get update

sudo apt-get install oracle-java8-installer