; -- install.iss --

[Setup]
AppName=FreeMarket
AppVersion=1.5
DefaultDirName={pf}\FreeMarket
DefaultGroupName=FreeMarket
UninstallDisplayIcon={app}\WebView.ico
Compression=lzma2
SolidCompression=yes
WizardImageFile=WebView-setup-icon.bmp
WizardImageStretch=no
OutputDir="..\..\build\deploy\bundles\WebView"
OutputBaseFilename=FreeMarket-setup
BackColor=clWhite
WizardImageBackColor=clWhite

[Files]
Source: "..\..\build\deploy\bundles\WebView\WebView.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\..\build\deploy\bundles\WebView\WebView.ico"; DestDir: "{app}"
Source: "..\..\build\deploy\bundles\WebView\*.dll"; DestDir: "{app}"
Source: "..\..\build\deploy\bundles\WebView\app\*"; DestDir: "{app}\app"; Flags: recursesubdirs
Source: "..\..\build\deploy\bundles\WebView\runtime\*"; DestDir: "{app}\runtime"; Flags: recursesubdirs

[Icons]
Name: "{group}\FreeMarket"; Filename: "{app}\WebView.exe"; Tasks: startmenu
Name: "{group}\Uninstall FreeMarket"; Filename: "{uninstallexe}"; Tasks: startmenu
Name: "{commondesktop}\FreeMarket"; Filename: "{app}\WebView.exe"; Tasks: desktopicon

[Tasks]
Name: desktopicon; Description: "Create a &desktop icon"; GroupDescription: "Additional icons:"
Name: startmenu; Description: "Create a &start menu icon"; GroupDescription: "Additional icons:"

