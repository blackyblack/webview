package webview;

import java.net.URL;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

class Browser extends Region
{

  final WebView browser = new WebView();
  final WebEngine webEngine = browser.getEngine();

  public Browser(URL path)
  {
    // load the web page
    if (path != null)
      webEngine.load(path.toString());
    else
      webEngine.loadContent("<H1>FreeMarket not installed properly</H1>");

    // add the web view to the scene
    getChildren().add(browser);

  }

  @Override
  protected void layoutChildren()
  {
    double w = getWidth();
    double h = getHeight();
    layoutInArea(browser, 0, 0, w, h, 0, HPos.CENTER, VPos.CENTER);
  }

  @Override
  protected double computePrefWidth(double height)
  {
    return 1024;
  }

  @Override
  protected double computePrefHeight(double width)
  {
    return 768;
  }
}

public class Main extends Application
{
  private Scene scene;
  static public NxtPassThread nxtpass = null;

  @Override
  public void start(Stage primaryStage)
  {
    try
    {
      URL path = ClassLoader.getSystemResource("index.html");
      URL iconPath = ClassLoader.getSystemResource("WebView.png");

      // create the scene
      primaryStage.setTitle("FreeMarket WebView");
      
      if(iconPath != null)
      {
        String path1 = iconPath.toString();
        Image icon1 = new Image(path1);
        
        if(icon1 != null)
        {
          primaryStage.getIcons().clear();
          primaryStage.getIcons().add(icon1);
        }
      }
      
      scene = new Scene(new Browser(path), 1024, 600, Color.web("#666970"));
      primaryStage.setScene(scene);
      primaryStage.show();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  @Override
  public void stop() throws Exception
  {
    if (nxtpass != null)
    {
      nxtpass.terminate();
    }
    super.stop();
  }

  public static void main(String[] args)
  {
    nxtpass = new NxtPassThread();
    nxtpass.start();
    launch(args);
  }
}
